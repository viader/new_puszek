package pl.fewbits.puszek.home;

import java.security.Principal;
import org.springframework.stereotype.Controller;
import pl.fewbits.puszek.core.rest.annotation.JsonRequestMapping;

@Controller
class HomeController {

  @JsonRequestMapping(value = "/")
  public String home(Principal principal) {
    return "index";
  }
}
