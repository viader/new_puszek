package pl.fewbits.puszek.login;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;
import org.springframework.web.bind.annotation.RestController;
import pl.fewbits.puszek.core.rest.annotation.JsonRequestMapping;

@RestController
public class LoginController {

  @JsonRequestMapping(value = "/login/")
  public Map<String, String> helloUser(Principal principal) {
    HashMap<String, String> result = new HashMap<>();
    result.put("username", principal.getName());
    return result;
  }
}
