package pl.fewbits.puszek.core.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

@Data
@Entity(name = "User")
public class User implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "nativeGenerator")
  @GenericGenerator(name = "nativeGenerator", strategy = "native")
  @Column(name = "id")
  private int id;

  @Column(name = "uid")
  private String uid;

  @Column(name = "email")
  private String email;

  @Column(name = "login")
  private String login;

  @Column(name = "name")
  private String name;

  @Column(name = "password")
  private String password;

  @Column(name = "isActive")
  private boolean isActive;
}
