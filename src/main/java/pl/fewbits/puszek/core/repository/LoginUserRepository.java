package pl.fewbits.puszek.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.fewbits.puszek.core.domain.User;

@Repository
@Transactional
public interface LoginUserRepository extends JpaRepository<User, Long> {

  User findByLoginOrEmail(String login, String email);
}
