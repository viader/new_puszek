package pl.fewbits.puszek.core.config.web.security.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.fewbits.puszek.core.config.web.security.domain.AppUserPrincipal;
import pl.fewbits.puszek.core.domain.User;
import pl.fewbits.puszek.core.repository.LoginUserRepository;

@Service
public class AppUserDetailsService implements UserDetailsService {

  @Autowired
  private LoginUserRepository userRepository;

  @Override
  public UserDetails loadUserByUsername(String loginOrEmail) throws UsernameNotFoundException {
    User user = userRepository.findByLoginOrEmail(loginOrEmail, loginOrEmail);
    if(user == null) {
      throw new UsernameNotFoundException(loginOrEmail);
    }
    return new AppUserPrincipal(user, loginOrEmail);
  }
}
