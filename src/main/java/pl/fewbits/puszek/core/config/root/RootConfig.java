package pl.fewbits.puszek.core.config.root;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import pl.fewbits.puszek.core.repository.LoginUserRepository;
import pl.fewbits.puszek.core.config.web.security.service.AppUserDetailsService;

@Configuration
@EnableJpaRepositories(basePackages = "pl.fewbits.puszek")
@EnableTransactionManagement
@ComponentScan(basePackageClasses = {RootConfig.class, LoginUserRepository.class, AppUserDetailsService.class})
public class RootConfig {
}