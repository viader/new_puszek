SET NAMES 'utf8';

DROP TABLE IF EXISTS `User`;
CREATE TABLE `User` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `isActive` bit(1) DEFAULT NULL,
  `login` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `uid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

INSERT INTO `User` (email, isActive, login, name, password) VALUES
  ('koliczyna@gmail.com', true, 'viader', 'Maciej Kalinowski', 'test'),
  ('olaop@gmail.com', true, 'ola', 'Ola Góźdź', 'test'),
  ('test@gmail.com', true, 'test', 'Tester', 'test');