package pl.fewbits.puszek.home;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import org.junit.Test;
import org.springframework.test.web.servlet.MockMvc;
import pl.fewbits.puszek.login.LoginController;

public class HomeControllerTest {

  @Test
  public void testHomePage() throws Exception {
    LoginController homeController = new LoginController();
    MockMvc mockMvc = standaloneSetup(homeController).build();
    mockMvc.perform(get("/api/home/"));
  }
}
