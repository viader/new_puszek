Puszek - backend project for mobile board games app.

To build project execute command in root dir:

./gradlew clean build

To run project deploy war to Tomcat server or use IntelliJ Idea plugin(https://www.jetbrains.com/help/idea/run-debug-configuration-tomcat-server.html)